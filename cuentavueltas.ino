#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include  <Chrono.h>
//Código para contar vueltas y tiempos en una pista de Slot con sensores de ultrasonidos.

//Manejo de la pantalla
//Primero creamos el objeto lcd  dirección  0x27, de 16 columnas y 2 filas
//(Pantalla LCD1602 de 16x2 caracteres, I2C PCF8574T)
LiquidCrystal_I2C lcd(0x27,16,2);  //

// Conectamos los pines Trigger y Echo del HC-SR04 al os pines D2 y D3 del Arduino Nano
const int trigPin = 2;
const int echoPin = 3;

//Variables
long duration;
float distance;
float sen2;
unsigned int cont;
unsigned int vuelta;

//Variable para medir el tiempo (de la librería Chrono.h)
Chrono chrono;


//Código de preparación, se ejecuta una vez.
void setup() {
  // Inicializar el LCD
  lcd.init();
  
  //Encender la luz de fondo.
  lcd.backlight();
  delay(100);

  //Ultrasonidos
pinMode(echoPin, INPUT); // El pin de Echo es una salidan
pinMode(trigPin, OUTPUT); // El pin de Trigger es una entrada
Serial.begin(9600); // Configuración del puerto serie a 9600 baudios
}

// Bucle infinito
void loop() {
	//Escribimos al inicio de la pantalla el tiempo que ha pasado.
  lcd.setCursor(0,0);
  lcd.print("Tiempo: ");
	lcd.print(cont);
  cont=chrono.elapsed();
	//Escribimos en la segunda línea el tiempo de la última vuelta
  // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  lcd.setCursor(0, 1);
  lcd.print("Mejor vuelta: ");
  lcd.print(vuelta);
  // Ultrasonidos
  // Ponemos el Trigger a nivel bajo
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Ponemos el Trigger a nivel alto 10 microsegundos
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  // Ponemos el Trigger a nivel bajo
  digitalWrite(trigPin, LOW);
	//Leemos el pin Echo, que nos da el tiempo en microsegyndos que ha tardado en volver la onda
  duration = pulseIn(echoPin, HIGH);
  // Calculamos la distancia, si es menor que 10 contamos una vuelta y limpiamos la pantalla
  distance = duration * 0.034 / 2;
  if(distance < 10) {
    chrono.restart();
    vuelta = cont;
    lcd.clear();
    delay(100);
    }
  // Imprimimos la información por puerto serie para debuggear
  Serial.print("Vuelta: ");
  Serial.println(vuelta);
   Serial.print("Tiempo: ");
  Serial.println(cont);
}
